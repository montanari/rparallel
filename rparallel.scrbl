#lang scribble/manual

@require[@for-label[rparallel
                    racket/base]]

@title{rparallel : Racket in parallel}
@author{Francesco Montanari}

License: LGPL-3.0-or-later

@defmodule[rparallel]

@section{Introduction}

This package provides high-level parallel forms.

The implementation relies on Racket futures. Due to coordination
overhead the library is only useful to parallelize computationally
instensive code. For instance, consider the following:

@racketblock[
(parallel-map sqrt (range 1000000))
]

Here the parallel form creates several futures whose overhead vastly
prevails over the relatively cheap @racket[sqrt] operation. This leads
to a much slower computation than the respective serial @racket[map]
call. To take advantage from multiple processor, larger chunks of
computation needs to be distributed over fewer futures, for instance:

@racketblock[
(parallel-let ((x (map sqrt (range 1000)))
               (y (map sqrt (range 1000) (range 1000000))))
              (append x y))
]

See @secref["effective-futures" #:doc '(lib
"scribblings/guide/guide.scrbl")] for further subtleties related to
the use of futures.

@section{Interface}

@defform[(parallel-values v ...)]{
Like @racket[values], but evaluates each @racket[v] in parallel.}

@defproc[(parallel-for-each [proc procedure?] [lst list?] ...+)
void?]{
Like @racket[for-each], but @racket[proc] calls are in parallel.}

@defform[(parallel-let ([id val-expr] ...) body ...+)]{
Like @racket[let], but all the expressions for the bindings are evaluated in
parallel.}

@defproc[(parallel-map [proc procedure?] [lst list?] ...+)
         list?]{
Like @racket[map], but @racket[proc] calls are in parallel.

See @hyperlink["https://docs.racket-lang.org/pmap/" "pmap : Parallel
map"] for alternative parallel map procedures that leverage both
Racket futures and places forms of parallelism.}
